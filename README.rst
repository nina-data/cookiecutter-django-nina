cookiecutter-django-nina
========================

`cookiecutter <https://cookiecutter.readthedocs.io/>`_ template for NINA projects based on Django.

`cruft <https://timothycrosley.github.io/cruft/>`_ can be used instead of cookiecutter to help maintaining projects updated.

Usage
-----

.. code-block:: sh

   cruft create https://gitlab.com/nina-data/cookiecutter-django-nina
